import './App.css';
import React from 'react';
import Calculator from './pages/Calculator';
import { 
	BrowserRouter as Router, 
	Routes, 
	Route } 
	from 'react-router-dom'

function App() {
  return (
	<div className="App">
		<Router>
			<Routes>
				<Route path="/" element={<Calculator />}/>
			</Routes>
		</Router>
	</div>
  );
}

export default App;
