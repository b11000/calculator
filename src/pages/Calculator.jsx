import React, { 
    // useEffect,
    // useReducer,
    useState } 
    from 'react'; 
import { Container, Form, Button } from 'react-bootstrap'

    // const reducer = (state, action) =>{
    //     switch (action.type){
    //         case "ADD":
    //             return {inputValue}
    //         case "SUBTRACT":
    //             return {inputValue}
    //         case "Multiply":
    //             return {}
    //         case "Divide":
    //             return {}
    //         default:
    //             return state;
    //     }
    // };
const Calculator = () => {
    const [input, setInput] = useState();
    const [input2, setInput2] = useState();
    const [output, setOutput] = useState(0);
    // const [result, setResult] = useState(0);
    
    // const [state, dispatch]=useReducer(reducer,{input1: 0, input2: 0 })

    // let onChange = (event) => {
    //     const newValue = event.target.value;
    //     setInputValue(newValue)
    // }
    const add = () => {
        let result = Number(input) + Number(input2)
        return setOutput(result)
    }
    
    const subtract = () => {
        let result = Number(input) - Number(input2)
        return setOutput(result)
    }
    
    const multiply = () => {
        let result = Number(input) * Number(input2)
        return setOutput(result)
    }

    const divide = () => {
        let result = Number(input) / Number(input2)
        return setOutput(result)
    }
    
    const reset = () => {
        setOutput(0);
        setInput('');
        setInput2('');
    };

    return (

    <Container>
        <Form>
            <Form.Group>
                <h1>Calculator</h1>
                <h2>{output}</h2>

                <input 
                    className='input'
                    value={input}
                    placeholder="Input Number"
                    onChange={(e)=>setInput(e.target.value)}
                    />

                <input 
                    className='input2'
                    value={input2}
                    placeholder="Input Number"
                    onChange={(e)=>setInput2(e.target.value)}
                    />

            </Form.Group>
            <Button onClick={() => add()}>Add</Button>

            <Button onClick={() => subtract()}>Subtract</Button>

            <Button onClick={() => multiply()}>Multiply</Button>

            <Button onClick={() => divide()}>Divide</Button>
            
            <Button onClick={() => reset()}>Reset</Button>
        </Form>
    </Container>
    )
};

export default Calculator;
